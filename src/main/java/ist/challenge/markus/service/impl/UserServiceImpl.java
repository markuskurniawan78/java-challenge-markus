package ist.challenge.markus.service.impl;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import ist.challenge.markus.entity.User;
import ist.challenge.markus.repository.UserRepository;
import ist.challenge.markus.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public void saveUser(User user) {
		// TODO Auto-generated method stub
		this.userRepository.save(user);
	}

	@Override
	public User findByUsername(String username) {
		// TODO Auto-generated method stub
		return this.userRepository.findByUsername(username);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getListUsers() {
		// TODO Auto-generated method stub
		String apiUrl ="https://swapi.py4e.com/api/people/?search=";
		HttpClient httpClient = HttpClient.newHttpClient();
		HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(apiUrl))
                .build();
		List<User> userList = new ArrayList<User>();
		try {
			 HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
			 if (response.statusCode() >= 200 && response.statusCode() < 300) {
	                ObjectMapper objectMapper = new ObjectMapper();
	                HashMap<String,Object> responseBody = objectMapper.readValue(response.body(), HashMap.class);	
	                List<HashMap<String, Object>> resultList = (List<HashMap<String, Object>>) responseBody.get("results");
	                int id = 1;
	                for (HashMap<String, Object> result : resultList) {
	                	String name = String.valueOf(result.get("name"));
	                	 User user = new User((long) id, name, name+id);
	                	 userList.add(user);
	                	 id++;
	                }	               
	            } else {
	            	throw new RuntimeException("Request failed with status code: " + response.statusCode());
	            }
		}catch (Exception e) {
            e.printStackTrace();
        }
		 return userList;
	}

	@Override
	public User findById(Long id) {
		// TODO Auto-generated method stub
		return this.userRepository.findById(id).get();
	}

}
