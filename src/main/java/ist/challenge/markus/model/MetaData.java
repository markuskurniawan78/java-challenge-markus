package ist.challenge.markus.model;

import lombok.Data;

@Data
public class MetaData {
	
	private int statusCode;
	private String message;
	
}
